FROM php:7.2-apache
RUN apt-get update && apt-get install vim -y
COPY php.ini /usr/local/etc/php/
COPY web/ /var/www/html/
